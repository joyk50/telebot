# -*- coding: utf-8 -*- 
import asyncio
import telepot #telegram API
import telepot.aio
import telepot.aio.helper
from telepot import glance
from telepot.aio.loop import MessageLoop
from telepot.aio.delegate import per_chat_id, pave_event_space, create_open, include_callback_query_chat_id
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
from libs.transmission import *

import sys
import re

#import feedparser  #rss parser

home_cmd_list = ['처음으로', '취소', '뒤로']

magnet_pattern = r'[Mm]agnet:\?xt=urn:btih:.*'

class TorrentManager(telepot.aio.helper.ChatHandler):
    def __init__(self, *args, **kwargs):
        super(TorrentManager, self).__init__(*args, **kwargs)
        self.agent = self.createAgent()
        self._stage = 0
    
    def setStage(self, menu):
        self._stage = menu
        
    def createAgent(self, agentType = 'transmission'):
        if agentType == 'transmission':
            return TransmissionAgent()
        raise('invalid torrent client')
        return None
    
    def addMagnet(self, text):
        magnets = re.findall(magnet_pattern, text)
            
        if len(magnets)>0:
            result = []
            for m in magnets:
                print(m)
                self.agent.downloadFromMagnet(m)
                result.append('magnet added: ' + m)
            return '\n'.join(result)
        else:
            error = '올바르지 않는 magnet url'
            print(error)
            return error
    
    async def on_chat_message(self, msg):
        # print(msg)
        if msg['text'] in home_cmd_list:
            self.setStage(0)
        
        if self._stage == 0 and re.search(magnet_pattern, msg['text']) != None:
            await self.sender.sendMessage(self.addMagnet(msg['text']))
            
        else:
            # inline keyboard callback
            if self._stage == 0:
                content_type, chat_type, chat_id = telepot.glance(msg)

                menu = InlineKeyboardMarkup(inline_keyboard=[
                    [InlineKeyboardButton(text='Magnet', callback_data='downloadFromMagnet')],
                    [InlineKeyboardButton(text='Torrent file', callback_data='downloadFromFile')],
                    [InlineKeyboardButton(text='Search', callback_data='searchTorrent')],
                ])
                await self.sender.sendMessage('다음 중 다운로드 방법을 선택', reply_markup=menu)
            elif self._stage == 1:
                magnet = msg['text']
                await self.sender.sendMessage(self.addMagnet(magnet))
                pass
            else:
                pass    
        
    async def on_callback_query(self, msg):
        query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')
        print('Callback Query:', query_id, from_id, query_data)

        if query_data == 'downloadFromMagnet':
            self.setStage(1)
            await self.sender.sendMessage('마그넷 URL을 넣어주세요.')
        elif query_data == 'downloadFromFile':
            self.setStage(2)
        elif query_data == 'searchTorrent':
            self.setStage(3)
        else:
            pass
        print('stage:', self._stage)
        
    async def on__idle(self, event):
        await self.sender.sendMessage('3분간 아무 입력이 없어 대화가 종료됩니다. 다시 시작하시려면 아무 입력을 해주세요.')
        self.close()
        
    def on_close(self, ex):
        # Save to database
        global propose_records
        propose_records[self.id] = (self._count, self._edit_msg_ident)
        
TOKEN = '130787551:AAF3Wr9xecl5tfP5jbK6gznRlk-OXJ_-ZJc'

bot = telepot.aio.DelegatorBot(TOKEN, [include_callback_query_chat_id(
    pave_event_space())(
    per_chat_id(), create_open, TorrentManager, timeout=180),
    ])

try:
    loop = asyncio.get_event_loop()
    loop.create_task( MessageLoop(bot).run_forever() )
    
    print('JoyK Bot Listening...')
    loop.run_forever()
    
except KeyboardInterrupt:
    print('Chatbot Clossed.')
        
        
'''
def example_handle(msg):
    # content_type, chat_type, chat_id = telepot.glance(msg)
    # print (content_type, chat_type, chat_id)
    pprint(msg)

class JoyKBot(telepot.helper.UserHandler):
    url_rss = 'https://torrentkim10.net/bbs/rss.php?k='
    GREETING = u'안녕하세요. JoyK NAS 토렌트 매니저 입니다.'
    INIT = u'/start'
    HELP1 = u'/help'
    HELP2 = u'/h'
    MENU = u'메뉴'
    MENU1 = u'검색'
    MENU1_1 = u'다운로드'
    MENU1_1_1 = u'서버로 직접 다운로드'
    MENU1_1_2 = u'토렌트 파일로 다운로드'
    MENU2 = u'관리'
    MENU2_1 = u'토렌트 삭제'
    BACK = u'뒤로'
    END = u'종료'
    YES = u'YES'
    NO = u'NO'

    navi = feedparser.FeedParserDict()
    pre_keyword =''
    mode = ''
    
    def __init__(self, seed_tuple, timeout):
        super(JoyKBot, self).__init__(seed_tuple, timeout)
        self.agent = self.createAgent(AGENT_TYPE)

        # keep track of how many messages of each flavor
        self._counts = {'chat': 0,
                'inline_query': 0,
                'chosen_inline_result': 0}
        self._answerer = telepot.helper.Answerer(self.bot)

    def on_message(self, msg):
        flavor = telepot.flavor(msg)
        self._counts[flavor] += 1
        print(self.id, ':', self._counts)
        
        # Have to answer inline query to receive chosen result
        if flavor == 'chat':
            content_type, chat_type, chat_id = telepot.glance(msg, flavor=flavor)
        #Check ID
        if not chat_id in VALID_USERS:
            print('Permission Denied. 봇 관리자에게 접근 권한 요청 할 것.')
            return
        if content_type is 'text':
            print(msg['text'])
            self.handle_command(msg['text'])
            return
        elif flavor == 'callback_query':
            print ('Callback query')
        elif flavor == 'inline_query':
            pass

    def compute_answer():
        query_id, from_id, query_string = telepot.glance(msg, flavor=flavor)
        articles = [{'type': 'article', 'id': 'abc', 'title': query_string, 'message_text': query_string}]
        self._answerer.answer(msg, compute_answer)
        return articles
        

    def handle_command(self, command):
        if command == self.INIT or command == self.MENU:
            self.menu()
        elif command == self.HELP1 or command  == self.HELP2:
            self.sender.sendMessage('\"/help\", \"/h\", 를 입력하시면 언제든지 도움말을 보실수 있습니다.\n아무 화면에서 \"메뉴\"를 입력하시면 초기 메뉴로 돌아갑니다.\n입력없이 3분이 지나면 봇과의 대화가 초기화 됩니다.')
        elif command == self.MENU2:
            self.tor_show_list()
        else:
            if self.mode == self.MENU1:
                if command == self.BACK:
                        self.menu()
                elif any(command in item for item in outList):
                    self.tor_download(command)
                else:
                    pass
            elif self.mode == self.MENU1_1:
                if command == self.MENU1_1_1:
                    self.mag_download(magnet)
                elif command == self.MENU1_1_2:
                    self.file_download(magnet)
                elif command == self.BACK:
                    self.tor_search(self.pre_keyword)
                else:
                    pass
            elif self.mode == self.MENU2:
                if command == self.BACK:
                        self.menu()
                else:
                    pass
            elif self.mode == self.MENU2_1:
                if command == self.BACK:
                    self.menu()
                else:
                    pass
            else:
                self.tor_search(command)

    def createAgent(self, agentType):
        if agentType == 'transmission':
            return TransmissionAgent()
        raise('invalid torrent client')
        return None

    def handle(self, msg):
        flavor = telepot.flavor(msg)
        # chat message
        if flavor == 'chat':
            content_type, chat_type, chat_id = telepot.glance(msg)
            print('Chat Message:', content_type, chat_type, chat_id)
        else:
            raise telepot.BadFlavor(msg)
        self.mode = self.INIT
        self.menu()

    def menu(self):
        self.mode = self.MENU
        show_keyboard = {'keyboard': [[self.MENU2], [self.HELP1]]}
        self.sender.sendMessage('아래의 메뉴에서 기능을 선택해 주세요. 키워드를 입력 하시면 검색을 시작합니다.', reply_markup=show_keyboard)

    def put_back_button(self, l):
        menulist = [self.BACK]
        l.append(menulist)
        return l

    def tor_search(self, keyword):
        self.mode = self.MENU1
        self.pre_keyword = keyword
        self.sender.sendMessage('키워드 검색 중: ' + keyword)
        rss = self.url_rss+parse.quote(keyword)
        # print (rss)
        self.navi = feedparser.parse(rss)
        # print(len(self.navi['entries']))

        global outList
        outList = []
        
        if not self.navi.entries:
            self.sender.sendMessage('검색결과가 없습니다. 다시 입력하세요.')
            self.mode=self.MENU
            return

        for (i,entry) in enumerate(self.navi.entries):
            if i == 20: break
            title = str(i+1) + '. ' + entry.title

            templist = []
            templist.append(title)
            outList.append(templist)

        show_keyboard = {'keyboard': self.put_back_button(outList)}
        self.sender.sendMessage('아래 메뉴에서 선택하세요.', reply_markup=show_keyboard)
            
    def tor_download(self, selected):
        self.mode = self.MENU1_1
        index = int(selected.split('.')[0]) - 1
        print ('index', index)

        global magnet
        magnet = self.navi.entries[index].link
        maglink = magnet.split('&dn=')[0]
        maghash = maglink.split('btih:')[1]
        self.sender.sendMessage('https://torrentkim3.net/magnet_search_newwin.php?hash='+maghash)

        show_keyboard = {'keyboard': [[self.MENU1_1_1], [self.MENU1_1_2], [self.BACK]]}
        self.sender.sendMessage('아래의 메뉴에서 기능을 선택해 주세요.', reply_markup=show_keyboard)
        
    def mag_download(self, magnet):
        self.agent.downloadFromMagnet(magnet)
        self.sender.sendMessage('서버에 다운로드를 시작합니다.')
        self.on_close()

    def file_download(self, magnet):
        self.sender.sendMessage('해당 기능 고치는 중...')
        #self.on_close()

    def tor_show_list(self):
        self.mode = self.MENU2
        self.sender.sendMessage('토렌트 리스트를 확인중..')
        outString = ''
        result = self.agent.getCurrentList()
        if not result:
            self.sender.sendMessage('진행중인 토렌트가 없습니다.')
            self.menu()
            return
        (outString, completedlist) = self.agent.filterCompletedList(result)
        self.completedlist = completedlist
        self.sender.sendMessage(outString)

        show_keyboard = {'keyboard': [[self.BACK]]}
        self.sender.sendMessage('아래의 메뉴에서 삭제할 토렌트를 선택해 주세요.', reply_markup=show_keyboard)

    def tor_del_list(self, command):
        self.mode = self.MENU2_1
        self.sender.sendMessage('완료된 항목을 자동 정리중..')
        for id in self.completedlist:
            self.agent.removeFromList(id)
        self.sender.sendMessage('완료')
        self.menu()
        
    def on_close(self, exception):
        hide_keyboard = {'hide_keyboard': True}
        self.sender.sendMessage('연결이 종료되었습니다. 검색하고 싶은 키워드를 입력해 주세요.', reply_markup=hide_keyboard)

'''


