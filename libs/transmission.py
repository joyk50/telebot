import os
import sys

AGENT_TYPE = 'transmission'
TRANSMISSION_USER = 'joyk'
TRANSMISSION_PASSWORD = 'joyk@4725'
TRANSMISSION_PORT = '9091'

class TransmissionAgent:
    def __init__(self):
        transmissionCmd = 'transmission-remote '
        if TRANSMISSION_USER:
            transmissionCmd = transmissionCmd + '-n ' + TRANSMISSION_USER
        if TRANSMISSION_PASSWORD:
            transmissionCmd = transmissionCmd + ':' + TRANSMISSION_PASSWORD
        transmissionCmd = transmissionCmd + ' '
        if TRANSMISSION_PORT:
            transmissionCmd = transmissionCmd + '-p ' + TRANSMISSION_PORT + ' '
            self.transmissionCmd = transmissionCmd

    def downloadFromMagnet(self, magnet):
        os.system(self.transmissionCmd + '-a ' + magnet)
    
    def getCurrentList(self):
        return os.popen(self.transmissionCmd + '-l').read()
    
    def filterCompletedList(self, result):
        outString = ''
        resultlist = result.split('\n')
        titlelist = resultlist[0]
        resultlist = resultlist[1:-2]
        completedlist = []
        for entry in resultlist:
            title = entry[titlelist.index('Name'):].strip()
            status = entry[titlelist.index('Status'):titlelist.index('Name')-1].strip()
            progress = entry[titlelist.index('Done'):titlelist.index('Done')+4].strip()
        if progress == '100%':
            titleid = entry[titlelist.index('ID'):titlelist.index('Done')-1].strip()
            completedlist.append(titleid)
            outString += '이름: '+title+'\n' + '상태:' + status + '\n'
        if progress:
            outString += '진행율:' + progress + '\n'
            outString += '\n'
            return (outString, completedlist)

    def removeFromList(self, id):
        os.system(self.transmissionCmd + '-t '+ id + ' -r')